# Example Frontend Automation for Gear4Music.com using WebdriverIO - Mocha Framework:

#### Author: 
Paul Joseph Farrell
#### Email: 
pauljosephfarrell89@gmail.com
#### LinkedIn: 
https://www.linkedin.com/in/faz540/

## Prerequisite/ Installation:
To clone this repository, you'll obviously need [Git](https://git-scm.com/download/win)
The above assumes you're using Windows.

You must have [Node.js](https://www.nodejs.org/) installed.

And you must also have [Java Development Kit](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed.

## Cloning The Repository:
Clone the repo and using terminal, 'change directory' into the cloned repo.

## Install Dependencies:
```
npm install
```

## To run all test files inside 'tests' folder, run the following command:
```
npm test
```