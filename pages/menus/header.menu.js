const waitUntil = require('../../utils/waits');
const ListingPage = require('../listing.page');

class Header {
    get logo() { return $('.home-link'); }
    get inputSearch() { return $('#srch-str'); }
    get buttonSearch() { return $('#header-search-button'); }
    get buttonInfo() { return $('[href="/information"]'); }
    get buttonLogIn() { return $$('[href="/auth/login"]')[0]; }
    get buttonBasket() { return $('[href="/basket/show"]'); }
    get buttonBasket() { return $('[href="#langModal"]'); }

    searchFor(searchTerm) {
        this.inputSearch.setValue(searchTerm);
        this.buttonSearch.click();
        return waitUntil.elementIsVisible(ListingPage.pageContent);
    };
};

module.exports = new Header();