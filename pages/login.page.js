const waitUntil = require('../utils/waits');
const accountPage = require('./account.page.js');

class LoginPage {
    // This is pretty messy as the page is re-using element IDs for web/mobile page views...
    // Hence I had to return ALL elements that share that ID, and select the last one
    // The last one is the desktop element, the first one would be the mobile view.
    get inputEmail() { return $$('#str_email')[1]; }
    get inputPassword() { return $$('#str_password')[1] ;}
    get buttonLogIn() { return $('//*[@id="login_form"]//*[@type="submit"]'); }

    open() {
        browser.url('/auth/login');
        return waitUntil.elementIsVisible(this.buttonLogIn);
    };

    login(email="faz.saber@gmail.com", password="BlackDiamond540") {
        this.inputEmail.setValue(email);
        this.inputPassword.setValue(password);
        this.buttonLogIn.click();
        return waitUntil.elementIsVisible(accountPage.buttonLogOut);
    };
};

module.exports = new LoginPage();