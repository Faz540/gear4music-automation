const waitUntil = require('../utils/waits');

class HomePage {
    get pageContent() { return $('.promo-main'); }

    open() {
        browser.url('/');
        return waitUntil.elementIsVisible(this.pageContent);
    };
};

module.exports = new HomePage();