class ListingPage {
    get pageContent() { return $('.results-block'); }
    get productResults() { return $$('.restricted-inv-notice-row'); }

    getNumberOfResults() {
        return this.productResults.length;
    };
};

module.exports = new ListingPage();