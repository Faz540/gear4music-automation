class AccountPage {
    // Desktop Selectors:
    get buttonLogOut() { return $$('[href="/auth/logout"]')[0]; }

};

module.exports = new AccountPage();