const expect = require('chai').expect;

// Testing Utilities:
const actions = require('../utils/actions');

// Pages:
const header = require('../pages/menus/header.menu');
const homePage = require('../pages/home.page');
const loginPage = require('../pages/login.page');
const accountPage = require('../pages/account.page');
const listingPage = require('../pages/listing.page');

describe('Smoke Tests', function() {
    beforeEach(function() {
        homePage.open();
        actions.deleteAllCookies();
    });
    it('successfully logs in', function() {
        loginPage.open();
        loginPage.login();
        const currentURL = browser.getUrl();
        expect(currentURL, "User was not re-directed to '/my-account'").to.contain('/my-account');
        expect(accountPage.buttonLogOut.isVisible(), 'Account Page: Log Out button is not displayed.').to.be.true;
    });
    it('performs a search and confirms results are returned', function() {
        header.searchFor('Red');
        const numberOfResults = listingPage.getNumberOfResults();
        expect(numberOfResults, 'No search results returned').to.be.greaterThan(0);
    });
});