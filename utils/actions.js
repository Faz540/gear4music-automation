class Actions {
    
    deleteAllCookies() {
        const cookies = browser.getCookie();
        return cookies.forEach(function(cookie) {
            return browser.deleteCookie(cookie.name);
        });
    };
};

module.exports = new Actions();