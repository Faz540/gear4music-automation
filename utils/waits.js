class WaitUntil {

    elementIsVisible(element) {
        return browser.waitForVisible(element.selector);
    };

    elementExists(element) {
        return browser.waitForExist(element.selector);
    };

};

module.exports = new WaitUntil();