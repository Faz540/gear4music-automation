exports.config = {
    baseUrl: 'https://gear4music.com',
    specs: [
        './tests/*.spec.js'
    ],
    maxInstances: 1,
    capabilities: [
        {
            browserName: 'chrome'
        }
    ],
    sync: true,
    logLevel: 'error',
    coloredLogs: true,
    deprecationWarnings: true,
    bail: 0,
    screenshotPath: './errorShots/',
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    services: ['selenium-standalone','screenshots-cleanup'],
    cleanScreenshotsFolder: {
        folder: 'errorShots',
        pattern: '*.png'
    },
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    afterSession: async function(){
        // workaround to make sure the chromedriver shuts down
        // https://github.com/webdriverio/wdio-selenium-standalone-service/issues/28
        await browser.end().pause(1000);
    },
    after: async function(){
        // workaround to make sure the chromedriver shuts down
        // https://github.com/webdriverio/wdio-selenium-standalone-service/issues/28
        await browser.pause(1000);
    }
}